import pytest


FILE = '/etc/passwd'
ROOT = 'root'


@pytest.mark.file
class TestFiles:

    def test_file_exists(self, host):
        current = host.file(FILE)
        assert current.exists

    def test_file_isfile(self, host):
        current = host.file(FILE)
        assert current.is_file

    def test_file_is_root(self, host):
        current = host.file(FILE)
        assert current.user == ROOT

    def test_file_has_root_id(self, host):
        current = host.file(FILE)
        assert current.uid == 0

    def test_file_mode(self, host):
        current = host.file(FILE)
        assert oct(current.mode) == '0o644'

    def test_file_contains_something(self, host):
        current = host.file(FILE)
        assert current.contains(ROOT)

    def test_file_inspect_content(self, host):
        current = host.file(FILE)
        assert ROOT in str(current.content)

    def test_file_inspect_content_easier(self, host):
        current = host.file(FILE)
        assert ROOT in current.content_string
