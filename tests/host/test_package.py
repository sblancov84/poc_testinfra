import pytest

PACKAGE = 'python3'
PIP_PACKAGE = 'testinfra'


@pytest.mark.current
@pytest.mark.package
class TestGroup:

    def test_python_is_installed(self, host):
        current = host.package(PACKAGE)
        assert current.is_installed

    def test_python_installed_version(self, host):
        current = host.package(PACKAGE)
        assert current.version.startswith('3.7')

    def test_pip_packages(self, host):
        current = host.pip_package.get_packages()
        assert PIP_PACKAGE in current

    def test_pip_old_packages(self, host):
        current = host.pip_package.get_outdated_packages()
        assert PIP_PACKAGE not in current
