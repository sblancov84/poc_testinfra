import pytest


@pytest.mark.group
class TestGroup:

    def test_root_group_exists(self, host):
        current = host.group('root')
        assert current.exists

    def test_root_group_gid(self, host):
        current = host.group('root')
        assert current.gid == 0
