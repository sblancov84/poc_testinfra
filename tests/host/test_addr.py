import pytest
from tests.common import for_all


@pytest.mark.addr
class TestAddr:

    def test_localhost_is_resolvable(self, host):
        name = "localhost"
        current = host.addr(name)
        assert current.is_resolvable

    def test_google_is_reachable(self, host):
        name = "google.es"
        current = host.addr(name)
        assert current.is_reachable

    def test_v4_and_v6_ips_are_available(self, host):
        name = "localhost"
        current = host.addr(name).ip_addresses
        expected = ["::1", "127.0.0.1"]
        assert for_all(lambda y: y in current, expected)

    def test_localhost_ips_v4_is_available(self, host):
        name = "localhost"
        current = host.addr(name).ipv4_addresses
        localhost_ipv4 = "127.0.0.1"
        assert localhost_ipv4 in current

    def test_localhost_ips_v6_is_available(self, host):
        name = "localhost"
        current = host.addr(name).ipv6_addresses
        localhost_ipv6 = "::1"
        assert localhost_ipv6 in current

    def test_print_port_is_open(self, host):
        name = "localhost"
        port = 8888
        current = host.addr(name).port(port)
        assert not current.is_reachable
