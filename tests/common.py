from functools import reduce


def for_all(expression, iterable):
    return reduce(lambda x, y: x and expression, iterable, True)


def same_set(current, expected):
    return set(current) == set(expected)
